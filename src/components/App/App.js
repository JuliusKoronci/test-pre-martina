import React, { Component } from 'react';
import logo from '../../logo.svg';
import './App.css';
import TableBody from '../Table/TableBody';
import TableHeader from '../Table/TableHeader';
import data from './data';

class App extends Component {
	_onEdit = (url) => {
		alert(url);
	};
	
	render() {
		return (
			<div className="App">
				<div className="App-header">
					<img src={logo} className="App-logo" alt="logo" />
					<h2>Welcome to React</h2>
				</div>
				<div className="App-intro">
					<h3>Ukazka tabulky</h3>
					<table className="table table-hovered table-responsive">
						<TableHeader />
						<TableBody data={data} handleEdit={this._onEdit} />
					</table>
				</div>
			</div>
		);
	}
}

export default App;
