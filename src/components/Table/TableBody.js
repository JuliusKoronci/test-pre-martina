// @flow
import React from 'react';
import PropTypes from 'prop-types';

const TableBody = ({ data, handleEdit }) => {
	console.log(data);
	return (
		<tbody>
		{data.map((item, i) => {
			return (
				<tr key={i}>
					<td>{item.name}</td>
					<td>{item.date}</td>
					<td>{item.url}</td>
					<td onClick={() => handleEdit(item.url)}><a href="#">edit</a></td>
				</tr>
			);
		})}
		</tbody>
	);
};

TableBody.defaultProps = {
	data: [],
};
TableBody.propTypes = {
	data: PropTypes.array.isRequired,
	handleEdit: PropTypes.func.isRequired,
};

export default TableBody;
