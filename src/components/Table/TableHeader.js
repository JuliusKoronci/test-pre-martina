// @flow
import React from 'react';

const TableHeader = () => (
	<thead>
	<tr>
		<th>meno</th>
		<th>datum</th>
		<th>url</th>
		<th />
	</tr>
	</thead>
);

export default TableHeader;
